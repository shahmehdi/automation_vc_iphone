package com.wsm.automation.vc.environment;

import com.rmn.automation.data.IMySqlDatabase;
import com.rmn.automation.data.SessionManager;
import com.rmn.automation.framework.environment.TestEnvironment;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by bboral on 6/24/14.
 */
public class VCUKTestEnvironment extends TestEnvironment {
    //Constructor
    public VCUKTestEnvironment(String applicationUrl, String hostUrl, IMySqlDatabase applicationDatabase, SessionManager sessionManager) {
        super(applicationUrl, hostUrl, applicationDatabase);
        this.sessionManager = sessionManager;
    }

    private SessionManager sessionManager;

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
}