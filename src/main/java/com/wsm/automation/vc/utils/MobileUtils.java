package com.wsm.automation.rmn.utils;

/**
 * Created by bboral on 6/25/14.
 */
public class MobileUtils {
    public static final int BEAT = 1000;

    public static final String VALID_EMAIL_1    = "tnguyen@rmn.com";
    public static final String VALID_USERNAME_1 = "All I Do Is Nguyen"; //username for above
    public static final String VALID_PASSWORD_1 = "whaleshark123"; //password for above

    public static final String VALID_EMAIL_2    = "rmn.testemail@gmail.com"; //an existing email that isn't affected by autocorrect
    public static final String VALID_USERNAME_2 = "rmntestemail"; //username for email 2

    public static final String INVALID_PASSWORD = "al3jfsl5sf2afs"; //random characters
}
