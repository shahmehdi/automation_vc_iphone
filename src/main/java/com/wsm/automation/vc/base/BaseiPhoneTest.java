package com.wsm.automation.vc.base;

import com.rmn.automation.framework.core.BaseWebDriverTest;
import com.rmn.automation.framework.core.TestContext;
import com.rmn.automation.framework.environment.TestApplicationContext;
import com.rmn.automation.framework.listeners.IAutomationWebDriverEventListener;
import com.rmn.automation.framework.util.AutomationUtils;
import com.rmn.automation.framework.util.AutomationWait;
import com.wsm.automation.vc.environment.VCUKTestEnvironment;
import com.wsm.automation.vc.page.PageFactory;
import com.wsm.automation.vc.page.HomePage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.lang.reflect.Method;


/**
 * User: mmerrell
 * Date: 6/20/13
 */
@Listeners( RmnSuiteListener.class )
public class BaseiPhoneTest extends BaseWebDriverTest<VCUKTestEnvironment,TestApplicationContext> {
    protected VCUKTestEnvironment envDetails;

    @BeforeMethod(alwaysRun = true,dependsOnMethods = "webDriverSetup")
    public void setUp(Method m) throws Exception {
        try{
            envDetails = TestContext.baseContext().getEnvironment();
        } catch(Throwable t) {
            takeScreenshot(m.getName(),m.getClass().getSimpleName());
            log.error("Error with NativeAppSetUp: " + AutomationUtils.getStackTrace(t));
            getContext().addExceptionForTest(m.getDeclaringClass().getSimpleName() + "." + m.getName(),t);
            throw t;
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown()  {
        getContext().removeContext();
    }

    @Override
    public IAutomationWebDriverEventListener getEventListener() {
        return null;
    }


    protected HomePage homePage;


    @BeforeMethod(alwaysRun = true,dependsOnMethods = "setUp")
    public void setUpIPhone(Method m) throws Exception {
        try{

            homePage = PageFactory.newPage(HomePage.class);
        } catch(Throwable t) {
            takeScreenshot(m.getName(),m.getClass().getSimpleName());
            log.error("Error with NativeAppSetUp: " + AutomationUtils.getStackTrace(t));
            getContext().addExceptionForTest(m.getDeclaringClass().getSimpleName() + "." + m.getName(),t);
            throw t;
        }

        //give the app a half-second on startup for the location modal to appear
        AutomationWait.sleep(1000);
    }
}
