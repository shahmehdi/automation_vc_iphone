package com.wsm.automation.vc.base;

import com.rmn.automation.data.GenericSessionManager;
import com.rmn.automation.data.GenericSessionManagerImpl;
import com.rmn.automation.framework.core.TestContext;
import com.rmn.automation.framework.environment.TestApplicationContext;
import com.wsm.automation.vc.environment.VCUKTestEnvironment;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Created by bboral on 7/16/14.
 */
public class RmnSuiteListener implements ISuiteListener {

    private static final String XML_LOCATION = "/applicationContext_iphone.xml";

    @Override
    public void onStart(ISuite suite) {
        RmnSuiteListener.initializeEnvironmentDetails();
    }

    @Override
    public void onFinish(ISuite suite) {

    }

    /**
     * Initialize all our app config values here so we have them when starting the run
     */
    public static void initializeEnvironmentDetails() {
        ApplicationContext appContext = new ClassPathXmlApplicationContext(XML_LOCATION);
        TestApplicationContext testContext = (TestApplicationContext)appContext.getBean("environment");
        TestContext.baseContext().setTestApplicationContext(testContext);
    }
}
