package com.wsm.automation.vc.page;

import com.rmn.automation.framework.webelements.DivWebElement;
import com.rmn.automation.framework.webelements.InputWebElement;
import com.rmn.automation.framework.webelements.LinkWebElement;
import org.openqa.selenium.By;

public class SignInPage extends AbstractPage{
    private LinkWebElement backButton = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]"));
    private LinkWebElement vcLogoLink = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAImage[2]"));
    private InputWebElement emailTextInput = new InputWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]"));
    private InputWebElement passwordTextInput = new InputWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIASecureTextField[1]"));
    private LinkWebElement forgotPasswordLink = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIAStaticText[1]"));
    private LinkWebElement signInButton = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableGroup[2]/UIAButton[1]"));
    private DivWebElement signInFaster = new DivWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableGroup[2]/UIAStaticText[1]"));
    private LinkWebElement facebookButton = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableGroup[2]/UIAButton[2]"));

    public LinkWebElement getBackButton() {
        return backButton;
    }

    public LinkWebElement getVcLogoLink() {
        return vcLogoLink;
    }

    public InputWebElement getEmailTextInput() {
        return emailTextInput;
    }

    public InputWebElement getPasswordTextInput() {
        return passwordTextInput;
    }

    public LinkWebElement getForgotPasswordLink() {
        return forgotPasswordLink;
    }

    public LinkWebElement getSignInButton() {
        return signInButton;
    }

    public DivWebElement getSignInFaster() {
        return signInFaster;
    }

    public LinkWebElement getFacebookButton() {
        return facebookButton;
    }
}
