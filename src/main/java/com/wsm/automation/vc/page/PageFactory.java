package com.wsm.automation.vc.page;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * This class constructs and returns a Page Object of the class specified
 * Created by bboral on 6/25/14.
 */
public class PageFactory {
    public static <T extends AbstractPage> T newPage(Class<T> clazz) {
        return getNewPageInstance(clazz);
    }

    private static <T> T getNewPageInstance(Class<T> clazz) {
        try {
            Constructor<T> ctor = clazz.getDeclaredConstructor();
            ctor.setAccessible(true);
            return ctor.newInstance();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new RuntimeException( String.format( "Attempting to construct page [%s] resulted in an InvocationTargetException: %s", clazz.getSimpleName(), e.getCause() ) );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException( String.format( "Attempting to construct page [%s] resulted in an NoSuchMethodException: %s", clazz.getSimpleName(), e.getCause() ) );
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException( String.format( "Attempting to construct page [%s] resulted in an InstantiationException: %s", clazz.getSimpleName(), e.getCause() ) );
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException( String.format( "Attempting to construct page [%s] resulted in an IllegalAccessException: %s", clazz.getSimpleName(), e.getCause() ) );
        }
    }
}
