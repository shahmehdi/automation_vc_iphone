package com.wsm.automation.vc.page;

import com.rmn.automation.framework.webelements.DivWebElement;
import com.rmn.automation.framework.webelements.LinkWebElement;
import org.openqa.selenium.By;

/**
 * Created by bboral on 12/5/14.
 */
public class HomePage extends AbstractPage {
    private DivWebElement logoVC = new DivWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAImage[1]"));
    private LinkWebElement connectWithFacebookButton = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]"));
    private LinkWebElement signInButton = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[2]"));
    private LinkWebElement signUpButton = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[3]"));
    private LinkWebElement privacyPolicyLink = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[4]"));
    private LinkWebElement termsAndConditionsLink = new LinkWebElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[5]"));

    public DivWebElement getLogoVC() {
        return logoVC;
    }

    public LinkWebElement getConnectWithFacebookButton() {
        return connectWithFacebookButton;
    }

    public LinkWebElement getSignInButton() {
        return signInButton;
    }

    public LinkWebElement getSignUpButton() {
        return signUpButton;
    }

    public LinkWebElement getPrivacyPolicyLink() {
        return privacyPolicyLink;
    }

    public LinkWebElement getTermsAndConditionsLink() {
        return termsAndConditionsLink;
    }
}
