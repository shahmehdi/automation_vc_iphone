package com.wsm.automation.vc.page;

import com.rmn.automation.framework.core.IAutomationFacade;
import com.rmn.automation.framework.core.TestContext;
import com.rmn.automation.framework.util.AutomationWait;
import com.wsm.automation.vc.environment.VCUKTestEnvironment;
import io.appium.java_client.AppiumDriver;

/**
 * Created by bboral on 6/25/14.
 */
public abstract class AbstractPage {
    protected VCUKTestEnvironment envDetails;

    protected AbstractPage() {
        this.initEnvironment();
    }

    /**
     * Populates the environment for this page by statically accessing TestContext
     * NOTE: This method is private at the moment, but we could consider making it public if we ever feel the need to change environments at runtime. It seems
     *  like we don't want it showing up in a list of available methods for every page if we can avoid it
     */
    private void initEnvironment() {
        this.envDetails = TestContext.baseContext().getEnvironment();
    }

    /**
     * Returns the driver
     * @return The current instance of the Webdriver
     */
    protected IAutomationFacade getWebDriver() {
        return TestContext.baseContext().getWebDriverContext().getWebDriver();
    }

    /**
     * Returns the AutomationWait for the current driver instance
     * @return An AutomationWait object
     */
    protected AutomationWait getWait() {
        return new AutomationWait( getWebDriver() );
    }

    /**
     * for using the appium java client methods directly
     * @return AppiumDriver
     */
    protected AppiumDriver getAppiumDriver() {
        return (AppiumDriver) getWebDriver().getBaseWebDriver();
    }
}