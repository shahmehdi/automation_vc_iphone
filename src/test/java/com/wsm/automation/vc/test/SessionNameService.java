package com.wsm.automation.vc.test;

import com.rmn.automation.data.ISessionName;
import com.rmn.automation.framework.util.AutomationUtils;

public class SessionNameService implements ISessionName {

    private static SessionNameService sessionNameService = new SessionNameService();

    @Override
    public String getTestName() {
        return AutomationUtils.determineTestName();
    }

    public static SessionNameService createInstance() {
        return sessionNameService;
    }
}
