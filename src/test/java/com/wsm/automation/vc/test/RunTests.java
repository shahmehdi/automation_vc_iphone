package com.wsm.automation.vc.test;

import com.rmn.automation.framework.listeners.AutomationSuiteListener;
import com.rmn.automation.framework.reporters.AutomationFailedReporter;
import com.rmn.automation.framework.reporters.AutomationTeamCityReporter;
import com.rmn.automation.framework.test.AbstractTestNGRunner;
import com.wsm.automation.vc.base.RmnSuiteListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class RunTests extends AbstractTestNGRunner {

    private static Logger log = LoggerFactory.getLogger(RunTests.class);

    private static final String CONFIGURE_FAILURE_POLICY = "continue";
    private static final String TEST_PACKAGE_TO_RUN = "com.wsm.automation.rmn.test.*";
    private static final String TEST_NAME = "Native App Tests";

    @Override
    protected String getDefaultTestPackageToRun() {
        return RunTests.TEST_PACKAGE_TO_RUN;
    }

    @Override
    protected void performStartups() {
        log.info("Performing RMN startups...");
        RmnSuiteListener.initializeEnvironmentDetails();
        log.info("RMN startups completed.");
    }

    @Override
    protected TestNG getTestNG() {
        TestNG tng = new TestNG();
        // Don't use default listeners as we want to use our own reporter and failed test rerun reporter
        tng.setUseDefaultListeners(false);
        return tng;
    }

    @Override
    protected XmlSuite getTestNGSuite() {
        XmlSuite suite = new XmlSuite();
        suite.setName("Suite");
        suite.setConfigFailurePolicy(RunTests.CONFIGURE_FAILURE_POLICY);

        return suite;
    }

    @Override
    protected XmlTest getTestNGTest(XmlSuite suite) {
        //Add the regression tests to the suite
        XmlTest regressionTest = new XmlTest( suite );

        log.info("Setting tests to Preserve Order");
        regressionTest.setPreserveOrder(Boolean.TRUE.toString());

        log.info("Setting Test Name: " + RunTests.TEST_NAME);
        regressionTest.setName(RunTests.TEST_NAME);

        return regressionTest;
    }

    @Override
    protected void addListeners() {
        // Not including retry listener
        listenersToAddToSuite.add(AutomationTeamCityReporter.class);
        listenersToAddToSuite.add(AutomationFailedReporter.class);
        listenersToAddToSuite.add(AutomationSuiteListener.class);
    }

    public static void main(String args[]) {
        RunTests testRunner = new RunTests();
        // Set to run serially for now
        testRunner.defaultThreadCount=1;
        testRunner.run();
    }
}
