package com.wsm.automation.vc.test.ios;

import com.wsm.automation.vc.base.BaseiPhoneTest;
import com.wsm.automation.vc.page.SignInPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class HomeTests extends BaseiPhoneTest {

    @Test(groups = {"home"})
    public void logoTest() {
        Assert.assertTrue(homePage.getLogoVC().isDisplayed());
    }

    @Test(groups = {"home"})
    public void signInTest() {
        homePage.getSignInButton().click();
        SignInPage signInPage = new SignInPage();

        signInPage.getEmailTextInput().sendKeys("vc.ldn.test@rmn.com");
        signInPage.getPasswordTextInput().sendKeys("VoucherCodes");
        signInPage.getSignInButton().click();
        Actions builder = new Actions(getWebDriver().getBaseWebDriver());
        builder.moveByOffset(486, 109).click().perform();
    }

}
